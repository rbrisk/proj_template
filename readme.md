# Project template

- Data is stored in `data` directory. Downloaded files are stored in `data/downloads`.
- Scripts are in `src`
- Log files are in `data/logs` because the working directory is `data`
- `report` directory is a subtree of `rpt_template`
